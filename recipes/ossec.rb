#stop all for redeploy

version = node[:ossec_version]

script 'ossec download' do
  interpreter "bash"
  code <<-EOH
    cd /var/tmp
    wget -U ossec https://bintray.com/artifact/download/ossec/ossec-hids/ossec-hids-#{version}.tar.gz
    wget -U ossec http://ossec.github.io/_downloads/ossec-hids-#{version}.tar.gz.sha256
    sha256sum -c ossec-hids-#{version}.tar.gz.sha256
    sum=$?
    if [[ $sum != 0 ]]
      then exit $sum
    fi
    tar -xf /var/tmp/ossec-hids-#{version}.tar.gz
    EOH
end

cookbook_file "/var/tmp/ossec-hids-#{version}/etc/preloaded-vars.conf" do
  source "preloaded-vars.conf"
  mode 0600
end


cookbook_file "/etc/rc.d/init.d/ossec" do
  source "ossec"
  mode 0700
end

script 'ossec install' do
  interpreter "bash"
  code <<-EOH
    cd /var/tmp/ossec-hids-#{version}
    ./install.sh
    chkconfig --add ossec
    /var/ossec/bin/agent-auth -m 10.1.0.247 -p 1515
    EOH
end


service 'ossec' do
  action [ :enable, :start ]
end
