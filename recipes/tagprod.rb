script 'tag_prod_volumes' do
  interpreter "bash"
  code <<-EOH
VOLUME=`aws ec2 describe-volumes --filter Name=attachment.instance-id,Values=#{node["opsworks"]["instance"]["aws_instance_id"]} Name=tag:opsworks:mount_point,Values=/var/lib/mongodb --region=#{node["opsworks"]["instance"]["region"]} --query 'Volumes[*].{ID:VolumeId}' | grep "ID" | cut -d":" -f2`
VOL_NOQUOTE=`echo $VOLUME | tr -d '"'`
TAG="Key"="prodDb","Value"="#{node[:prodDb]}"
aws ec2 create-tags --resources $VOL_NOQUOTE --tags $TAG --region #{node["opsworks"]["instance"]["region"]} 
    EOH
end
