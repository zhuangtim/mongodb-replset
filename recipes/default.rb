stackname = node[:opsworks][:stack][:name]
layername = node[:opsworks][:instance][:layers].first
nodelist = search(:node, 'layers:"#{layername}"')
hostname = node[:opsworks][:instance][:hostname]
Chef::Log.info("Hostname: #{hostname}")
Chef::Log.info("Nodes detected: #{nodelist}")

# insure correct permissions
directory '/var/lib/mongodb' do
  owner 'mongod'
  group 'mongod'
  mode '0755'
  action :create
end

# create mongodb.conf with appropriate settings for replset
template "/etc/mongod.conf" do
  source "mongodbreplset.conf.erb"
  owner "root"
  group "root"
  mode 0644
  variables(
    :stackname => stackname
    )
  action :create
end

service "mongod" do 
  supports :restart => true
  action :start
  subscribes :restart, 'template[/etc/mongodb.conf]', :immediately
end

#Logs rotation
cookbook_file "/etc/logrotate.d/mongodb" do
  source "mongodb_logrotate"
  mode 0644
end

# Initiates the replset only on mondb1 host
# It will retry up to 40 times, 15 sec between tries, 10 minutes worth of trying 
# All the nodes have to be up for it to initiate
script 'initiate_replset' do
  subscribes :run, 'service[mongod]'
  interpreter "ruby"
  Chef::Log.info("Running replset initiate script")
  code <<-EOH 
    require 'json'
    retries = 0
    until  JSON.parse(`mongo localhost:27017 --quiet --eval "JSON.stringify(rs.status())"`)['ok'] == 1 || retries == 40 
      # Make this more readable
      `mongo localhost:27017 --quiet --eval "JSON.stringify(db.adminCommand({'replSetInitiate' : {_id: '#{stackname}', members: [{_id: 1, host: 'mongodb1.#{stackname}.qdoba.bottlerocketservices.com:27017'},{_id: 2, host: 'mongodb2.#{stackname}.qdoba.bottlerocketservices.com:27017'},{_id: 3, host: 'mongodb3.#{stackname}.qdoba.bottlerocketservices.com:27017'}]}}))"`
      retries += 1
      sleep(15)
    end
    EOH
  action :nothing
end
