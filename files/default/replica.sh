#!/bin/bash
# call rs.initiate({...})
cfg="{
    _id: 'qdoba',
    members: [
        {_id: 1, host: 'mongodb1:27017'},
        {_id: 2, host: 'mongodb2:27017'},
        {_id: 3, host: 'mongodb3:27017'}
    ]
}"
mongo localhost:27017 --eval "JSON.stringify(db.adminCommand({'replSetInitiate' : $cfg}))"
